# Innov8rs

![Inno](/docs/img/tr/inno.png)

Tribe by Innovators

Tribe offers you a Personalized interactive map with BLE (Bluetooth Low Energize) Beacon network, 
that gives you location and informtion of the resort Facility.
It also comes with a visitor mobile app for the resort, giving the location tracking of visitors.
For the resort management, a realtime view of visitor density of all activity with a broadcast system 
which make it possible to interact with their visitors.
We give a service to resort, hotel owners or business people to put their business on the MAP. 
In other for their visitor to enjoy their stay at the fullest, giving them the guidance they need.

Our system is a platform for add-ons like:
- Book
- Pay
- Free/Busy
- Activity Calender

Tribe makes [your ressort] great again!

![logo](/docs/img/tr/logo.jpg)


