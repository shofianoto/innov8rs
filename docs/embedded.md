# 2. Embedded solution

<Describe the embedded solution for your project here.>

### Using the ESP32 as a Beacon.

ESP32 is really a powerful IoT device having inbuilt support for Bluetooth and WiFi. The ESP32 is an advanced version of it's predecessor ESP8266 with extra features like RAM, ROM, GPIO Pins etc. The ESP32 module supports both classic Bluetooth and Bluetooth Low Energy (BLE).

### Programming ESP32 to work as BLE iBeacon

As we know the BLE can operate in two different modes:
- Server mode
- Client mode

## 2.1 Objectives

<Provide background and reasoning what needs to be solved here, why this solution was choosen and what steps need to be taken>

## 2.2 Steps taken

**Steps we have taken are:**

- We Opened Arduino IDE and Select “ESP32 Dev Module”. (we installed the ESP32 Board Package first, else you could not select the board in the Arduino IDE)
- Go to File > Examples > ESP32 BLE Arduino > BLE_iBeacon
- Open “BLE_iBeacon” Sketch.

![sketch](img/tr/sketch.jpg)

The ESP32 BLE libraries are included which contains many functions used to make ESP32 in different configurations such as BLE Client or BLE Server.

```
#include "BLEDevice.h"
#include "BLEUtils.h"
#include "BLEServer.h"


The whole code that we are using in this case is as followed:


#include "sys/time.h"
#include "BLEDevice.h"
#include "BLEUtils.h"
#include "BLEServer.h"
#include "BLEBeacon.h"
#include "esp_sleep.h"

#define GPIO_DEEP_SLEEP_DURATION     10  // sleep x seconds and then wake up
RTC_DATA_ATTR static time_t last;        // remember last boot in RTC Memory
RTC_DATA_ATTR static uint32_t bootcount; // remember number of boots in RTC Memory

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/
BLEAdvertising *pAdvertising;   // BLE Advertisement type
struct timeval now;

#define BEACON_UUID "87b99b2c-90fd-11e9-bc42-526af7764f64" // UUID 1 128-Bit (may use linux tool uuidgen or random numbers via https://www.uuidgenerator.net/)

void setBeacon() {

  BLEBeacon oBeacon = BLEBeacon();
  oBeacon.setManufacturerId(0x4C00); // fake Apple 0x004C LSB (ENDIAN_CHANGE_U16!)
  oBeacon.setProximityUUID(BLEUUID(BEACON_UUID));
  oBeacon.setMajor((bootcount & 0xFFFF0000) >> 16);
  oBeacon.setMinor(bootcount & 0xFFFF);
  BLEAdvertisementData oAdvertisementData = BLEAdvertisementData();
  BLEAdvertisementData oScanResponseData = BLEAdvertisementData();

  oAdvertisementData.setFlags(0x04); // BR_EDR_NOT_SUPPORTED 0x04

  std::string strServiceData = "";

  strServiceData += (char)26;     // Len
  strServiceData += (char)0xFF;   // Type
  strServiceData += oBeacon.getData();
  oAdvertisementData.addData(strServiceData);

  pAdvertising->setAdvertisementData(oAdvertisementData);
  pAdvertising->setScanResponseData(oScanResponseData);
}

void setup() {

  Serial.begin(115200);
  gettimeofday(&now, NULL);
  Serial.printf("start ESP32 %d\n", bootcount++);
  Serial.printf("deep sleep (%lds since last reset, %lds since last boot)\n", now.tv_sec, now.tv_sec - last);
  last = now.tv_sec;

  // Create the BLE Device
  BLEDevice::init("ESP32 as iBeacon");
  // Create the BLE Server
  BLEServer *pServer = BLEDevice::createServer(); // <-- no longer required to instantiate BLEServer, less flash and ram usage
  pAdvertising = BLEDevice::getAdvertising();
  BLEDevice::startAdvertising();
  setBeacon();
  // Start advertising
  pAdvertising->start();
  Serial.println("Advertizing started...");
  delay(100);
  pAdvertising->stop();
  Serial.printf("enter deep sleep\n");
  esp_deep_sleep(1000000LL * GPIO_DEEP_SLEEP_DURATION);
  Serial.printf("in deep sleep\n");
}

void loop() {
}
```
**Positioning the ESP32 iBeacon on the MAP**

We need to use three ESP32 modules to run the application, since this is required to do the trilateration.

First we installed the Mosquitto MQTT Broker (server)

```
sudo apt install mosquitto
```

NPM for running the web dashboard

```
sudo apt install npm
```

We have installed some extra libraries via NPM in order to see an error if so, when we run the dashboard.
using the following command:

```
npm install chalk
```

We are using the Arduino BLE and MQTT libraries:

- PubSubClient
- ESP32 BLE Arduino

There were problems in sending too much data. So we checked the serial console for an ESP32 and make sure that it prints out PUB Result: 1.

If it prints PUB Result: 0 there may be a problem transmitting the json data to the MQTT server. We fixed this by changing MQTT_MAX_PACKET_SIZE in the PubSubClient library to 2048.

**Setup and running the Mosquitto MQTT Broker (server)**

First edit the file mosquitto-demo.conf. Set the path to acl files, like:

```
acl_file /home/w3/Downloads/iBeacon-indoor-positioning-demo/mosquitto_acl.config
password_file /home/w3/Downloads/iBeacon-indoor-positioning-demo/mosquitto_pw.config
```

Start mosquitto with the following command in the innov8rs folder itself:

```
mosquitto -v -c ./mosquitto-demo.conf
```

MQTT users are "station" and "dashboard", and all passwords are "bledemo".

### ESP32 stations

Edit the file credentials.h and change wifi ssid and password, and also change mqttServer to the IP of the computer running the mosquitto server.

![arduinocred](img/tr/arduinocred.jpg)

Upload the Arduino sketch ESP32_station.ino to your ESP32.

![uploading](img/tr/uploading.jpeg)

Place the ESP32 modules around the area where you want to do positioning, preferably 10-15 meters appart and also in wifi range.

ESP32 modules will work as iBeacon monitoring stations, reporting all found bluetooth beacons to the MQTT topic, with their MAC address and RSSI.

**Final architecture**

![architect](img/tr/architect.jpeg)

**Web dashboard**

The dashboard is a simple React app, connecting to the mosquitto MQTT server and showing each beacon on screen.

To run the dashboard from a terminal go to the dashboard folder and run npm start

```
cd dashboard/
npm start
```

The browser window will automatically open the dashboard react app, and go to http://localhost:3000/ (or you can just tick the adress of the server on the raspberry pi)

Stations and beacons will show up on our map when the three ESP32 stations are connected.

![beacontest](img/tr/beacontest.jpeg)

Beacontest with the raspberry pi

![map](img/tr/map.jpg)

The stations are the people and the beacons are the circles

### Making the app:

One of the most important things that any IOT project requires is a database to store the values, results and do some computation on them.

For our project we are using google spreadsheet to store and collect data to build the interactive mobile app on Android and Apple devices. The envirement that we are using is glide.
You can install the app by scanning the QRcode or by following the link: [TRIBE](https://tribe-innov8rs.glideapp.io/)

![qrcode](docs/img/tr/qrcode.jpg)

This will open the app and you can than register yourself.

![start](docs/img/tr/start.jpg)

We are linking the app with the localhost, which is running from the RaspberryPi. And because we have made the trilateration with the ESP32 beacons, we can see all the people that are in the area of the beacons. Determaning the location of everyone inside the radius of the litteration (with coordinates calculations).

![map+](docs/img/tr/map+.jpg)

There is also the ability to pay with mope by link for now:

![pay1](docs/img/tr/pay1.jpg)

### Digital Manufacturing:

We have made our own 3D design in tinkercad, generate the gcode with cura and printed the enclosure for our ESP32 with the 3D printer using the PLA filament. For the raspberry p enclosure we build it ourselves.

![benclo0](docs/img/tr/benclo0.jpg)

## 2.4 Testing & Problems

<provide tests performed, describe problems encountered and how they were solved (or not)>

## 2.5 Proof of work

< Add video and/or photos of your tests and proof it works!>

## 2.6 Files & Code

<Add your embedded code and other relevant documents in a ZIp file under the /files folder and link here.>

## 2.7 References & Credits

<add all used references & give credits to those u used code and libraries from>