# 4. Business & Marketing

<Describe the embedded solution for your project here.>

Suriname is a beautiful country with a lot of nice resorts, places, hotspots, big hotels where people like to go, but all of them have one common problem, they all lack Guidance.
Most of the time visitors don't know how to get fimiliar with the place, because they don't have the slidest idea what these places have to offer. 
So we give the service to these resorts owners an interactive map, where visitors can find themself in the map and can see which activities (sauna, pool, bar etc) or events (stages with different kinds of bands/Dj's etc) there are on the map.
Every resort must have their own map, which can be uploaded in our system. 

## 4.1 Objectives

<Provide background and reasoning what needs to be solved here, why this solution was choosen and what steps need to be taken>

e.g. of an user expierence

- clueless: Visitors are in a resort, they arrive there, but have no clue where to go or how to start.
- Register: Visitors have to go to the lobby, asking where are where, but forget it after an hour
- Book: it's mendatory to book at the lobby for every event/activity at the resort
- Events/activity availability: what activities do the resort have and what time is it available (kitchen opens at etc )
- Interaction with the resort
- visitor tracking: where are they, which activity is most visited one

## 4.3 Ananlyze your market & segments

<Identify Customer segments, value proposition, channels and identify monitizable pains>

#TARGET CUSTOMER#
- RESORT OWNERS
- HOTEL OWNERS
- ALL PLACES WITH POTENTIAL VISITORS

#CUSTOMER CHANNEL#
- MOBILE APP
- SOCIAL MEDIA
- PROMOTIONS

#REVENUE STREAM#
- DIRECT SELLING
- ONLINE
- RESORT DEMO’S

## 4.4 Build your business canvas

<With the help or (Canvanizer Online)[https://canvanizer.com/choose/business-model-canvases] create a business canvas for your project>

https://canvanizer.com/canvas/w2PxuimRQG8CV

## 2.5 Build your pitch deck

< 1. Build your Problem Solution Pitch for Idea Pitching!>
< 2. Build pitch deck for products/investor pitch>

https://docs.google.com/presentation/d/1H-FIwNTkSwomOy7MMpMZessw0HpeiyWvzj6HpUgi5fA/edit#slide=id.gadc2eae3ff_2_213

## 4.6 Make a project Poster

<Build a poster for your project describing in a nutshell what it is for expo purposes>

![](https://image.slidesharecdn.com/poster-iot-virtualization-v4-160909161427/95/iot-virtualization-poster-1-638.jpg?cb=1473437776)



## 4.7 Files & Code

<Add your embedded code and other relevant documents in a ZIp file under the /files folder and link here.>

## 4.8 References & Credits

<add all used references & give credits to those u used code and libraries from>